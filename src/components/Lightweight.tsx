import { CandlestickSeriesPartialOptions, ChartOptions, ColorType, DeepPartial, createChart } from 'lightweight-charts';
import { useEffect, useRef, useState } from 'react';
import api from '../api';
import { socket } from '../socket';

export const Lightweight = (props: any) => {
  const [data, setData] = useState<any[]>([])
  const { currency } = props

  const chartContainerRef = useRef<HTMLDivElement | null>(null);
  const chartOptions: DeepPartial<ChartOptions> = {
    layout: {
      background: { type: ColorType.Solid, color: 'white' },
      textColor: 'black',
    },
  };
  const candlestickSeriesOption: CandlestickSeriesPartialOptions = {
    upColor: '#26a69a', downColor: '#ef5350', borderVisible: false,
    wickUpColor: '#26a69a', wickDownColor: '#ef5350',
  }

  const getData = () => {
    api.get(`twelvedata/data?base=${currency}&quote=USD&exchange=Binance&interval=1d&start_date=2023-10-01`).then(res => {
      setData(res.data);
    })
  }

  useEffect(() => {
    if (!currency) return;
    getData()
    socket.on('connect', function () {
      //
    });
    socket.on('events', function (event: any) {
      getData()
    });
    socket.on('exception', function (data: any) {
      console.log('event', data);
    });
    socket.on('disconnect', function () {
      console.log('Disconnected');
    });
  }, [currency])

  useEffect(() => {
    if (!chartContainerRef.current || !currency || !data.length) return;

    const chart = createChart(chartContainerRef.current, chartOptions)

    chart.applyOptions({ width: chartContainerRef.current.clientWidth });
    chart.timeScale().fitContent();

    const candlestickSeries = chart.addCandlestickSeries(candlestickSeriesOption);
    candlestickSeries.setData(data)

    const handleResize = () => {
      if (!chartContainerRef.current) return;
      chart.applyOptions({ width: chartContainerRef.current.clientWidth });
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
      chart.remove();
    };
  }, [currency, data])

  return (
    <div ref={chartContainerRef} style={{
      width: '100%', height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    }}></div>
  )
}

export default Lightweight;
