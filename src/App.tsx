import './App.css';
import { Layout, Menu, Button, theme } from 'antd';
import Lightweight from './components/Lightweight';
import { useEffect, useState } from 'react';
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
} from '@ant-design/icons';
import api from './api';

const { Header, Sider, Content } = Layout;

const App: React.FC = () => {
  const [collapsed, setCollapsed] = useState<boolean>(false);
  const [currencies, setCurrencies] = useState<any[]>([])
  const [currency, setCurrency] = useState<string>('BTC')

  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();

  useEffect(() => {
    api.get('currencies').then((res: any) => {
      setCurrencies(res.data.map((c: any) => ({ key: c.symbol, label: c.name })))
    })
  }, [])

  const changeCurrency = (e: any) => {
    setCurrency(e.key)
  }

  return (
    <Layout style={{ height: '100vh' }}>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="demo-logo-vertical" />
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={[currency]}
          items={currencies}
          onSelect={changeCurrency}
        />
      </Sider>
      <Layout>
        <Header style={{ padding: 0, background: colorBgContainer }}>
          <Button
            type="text"
            icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
            onClick={() => setCollapsed(!collapsed)}
            style={{
              fontSize: '16px',
              width: 64,
              height: 64,
            }}
          />
        </Header>
        <Content
          style={{
            margin: '24px 16px',
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
            borderRadius: borderRadiusLG,
          }}
        >
          <Lightweight currency={currency} />
        </Content>
      </Layout>
    </Layout>
  )
}

export default App;
